# Devinette_d_un_nombre



## Objectif
Créer un mini-jeu de devinette en Python en utilisant Git. Le programme choisit un nombre aléatoire entre 1 et 100. L'utilisateur doit deviner le nombre en moins de 10 essais. L’objectif est d’apprendre les bases de la gestion de versions et se familiariser avec un workflow Git simple. Veuillez lire attentivement les consignes avant d’y répondre. N’oubliez pas de rendre votre travail public sur Gitlab.

## Consignes 
### Créer un nouveau projet sur Gitlab nommé « Devinette d'un nombre » et le cloner sur votre ordinateur.
![S-1](Screenshot/Capture d'écran 2024-03-11 133439.png)

### Créer dans le dossier cloné un fichier « main.py » et écrire le programme du jeu.
![S-2](Screenshot/Capture d'écran 2024-03-11 133514.png)
![S-3](Screenshot/Capture d'écran 2024-03-11 133621.png)

### Propagez les modifications sur le dépôt distant avec un message de commit clair et précis.
![S-4](Screenshot/Capture d'écran 2024-03-11 133620.png)

### Créez une nouvelle branche « dev » et développer dans cette branche la possibilité que le programme affiche des indices pour aider l'utilisateur (plus grand, plus petit) et qu’il affiche à la fin après combien d’essais.
![S-5](Screenshot/Capture d'écran 2024-03-11 133709.png)
![S-6](Screenshot/Capture d'écran 2024-03-11 142729.png)

### Sauvegarder les modifications sur le dépôt distant.
![S-7](Screenshot/Capture d'écran 2024-03-11 142624.png)

### Procéder à la fusion de la banche « dev » avec la branche principale.
![S-8](Screenshot/Capture d'écran 2024-03-11 133755.png)


