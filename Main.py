# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 12:10:23 2024

@author: HP
"""
import random
reponse=random.randint(0,100)
Essais=10
while Essais>0:
    Essais-=1
    nombre=int(input("Entrez un nombre compris entre 0 et 100:"))
    if nombre==reponse:
        print("Felicitations vous avez trouvez la reponse en",10-Essais,"essais")
        break
    elif nombre < reponse:
            print("Le nombre à deviner est plus grand.")
    else:
            print("Le nombre à deviner est plus petit.")

if Essais==0:
    print("Désolé le nombre d'essais est fini, la reponse était:",reponse)
